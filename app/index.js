var React=require('react');
var ReactDOM=require('react-dom');
var products= require('./products');



class Result extends React.Component
{
    render()
    {
        return (
          <div >
        <h2>{this.props.product.name}</h2>
        <p>{this.props.product.price}</p>
        <p>{this.props.product.description}</p>
          </div>  
        );
    }
}



class Results extends React.Component {
  
    constructor(props)
    {
        super(props);
        this.state={
            foundProducts:props.products
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps)
    {
        var foundProduct=nextProps.products.filter(product =>
            {
                return product.name.match(nextProps.query)
                ||
                product.description.match(nextProps.query);
            });

            this.setState(
            {
                foundProducts:foundProduct
            }
            )
    }

    render() {
        
        return (
            <div className="results">
              {this.state.foundProducts.map((product, i) => {
                return (
                  <Result product={product} key={i} />
                )
              })}
            </div>
          )
        
    }
}






class Searchit extends React.Component {
    
    searchEvent(event)
    {
        this.props.onType(event.target.value);
        
    }
    render() {
        return (
            <div id="search-bar">
                <input onChange={this.searchEvent.bind(this)} placeholder="Serch Here"/>
            </div>
        )
    }
}







class Search extends React.Component {
constructor(props)
{
    super(props);
    this.state={
        query:""
    }
}

onType(query)
{
    this.setState({
        query:query.trim()
    })
   

}

    render() {
        return (
            <div className="search">
              <Searchit onType={this.onType.bind(this)} />
                <Results products={this.props.products} query={this.state.query}/>
            </div>
        )
    }
}



ReactDOM.render(<Search products={products} />,
    document.getElementById('app'));